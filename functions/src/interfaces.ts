interface CLCUser {
    f_name: string,
    l_name: string,
    permission: string,
    division: string
}

interface Menu {
    scr_id: string,
    title: string
}

interface CLCMenu {
    menu_items: Array<Menu>
}

interface DetailUser{
    empCode: string,
    personCode: string,
    firstName: string,
    firstName2: string,
    lastName: string,
    lastName2: string,
    personName: string,
    personName2: string,
    statusCode: string,
    statusName: string,
    statusName2: string,
    workYear: number,
    workYearInt: number,
    insuranceType: number,
    insuranceTypeName: string,
    insuranceTypeName2: string,
    locCode: number,
    occCode: number,
    occName: string,
    occName2: string,
    socInsYear: number,
    enteredDate: Date,
    retiredDate: Date,
    sackedDate: Date,
    createdBy: number,
    createdDatetime: Date,
    modifiedBy: number,
    modifiedDatetime: Date,
    companyCode: string,
    returnDate: Date,
    empTypeId: number,
    empTypeName: string,
    empTypeName2: string,
    positionCode: string,
    positionName: string,
    positionName2: string,
    rankId: number,
    rankName: string,
    rankName2: string,
    chartCode: string,
    chartName: string,
    chartName2: string,
    registerCode: string,
    workEmail: string,
    workPhone: string,
    workIntPhone: string,
    firstDate: Date,
    workYearSector: number,
    maternityYear: number,
    intTermabsYear: number,
    lastChartYear: number,
    lastPositionYear: number,
    locName: string,
    locName2: string,
    mainEmpDate: Date,
    sexCode: number,
    sexName: string,
    sexName2: string,
    startDate: Date,
    regionCode: number,
    regionName: string,
    regionName2: string,
    createdUserName: string,
    modifiedUserName: string,
    adlogOnPc: string,
    phone: string,
    permission: string
}

interface Attendance {
    att_date: string,
    att_awanting_minutes: string,
    att_end_time: string,
    att_start_time: string,
    ref: string
}

interface UIDName {
    docID: string,
    firstName: string,
    lastName: string
}

interface RequestAttendance {
    UID: string,
    startDate: string,
    term: number,
    termUnitName: string,
    termUnitId: string,
    leaveTypeName: string,
    leaveTypeId: number,
    isSalary: string,
    statusName: string,
    statusCode: string,
}

interface LastIdOfVacation {
    count: number
}

