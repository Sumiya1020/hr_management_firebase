import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import './interfaces';

admin.initializeApp();

// Нэвтэрсэн хэрэглэгчийн эрхэд харгалзах menu-г буцаана
export const getMenus = functions.https.onRequest((request, response) => {
    if (request.body.uid) {
        const uid = request.body.uid as string;
        const promiseOfUser = admin.firestore().collection('clc_user').doc(uid).get();
        const promiseOfUserInvoked = promiseOfUser.then(snapshot => {

            functions.logger.info('snapshot', snapshot);

            const dataOfUid = snapshot.data() as CLCUser;
            functions.logger.info('dataOfUid', dataOfUid);
            const promiseOfMenu = admin.firestore().collection('clc_menu').doc(dataOfUid.permission).get();
            const promiseOfMenuInvoked = promiseOfMenu.then(snapshot1 => {

                response.send(snapshot1.data());
            });
            promiseOfMenuInvoked.catch(error => {
                console.log(error);
                response.status(500).send(error);
            });

        });
        promiseOfUserInvoked.catch(error => {
            response.status(500).send(error);
        });
    } else {
        response.status(500).send('UID олдсонгүй.');
    }
});

export const getDetailUser = functions.https.onRequest((request, response) => {
    if (request.body.uid) {
        const uid = request.body.uid as string;

        const promiseOfUser = admin.firestore().collection('clc_user').doc(uid).get();
        const promiseOfUserInvoked = promiseOfUser.then(snapshot => {

            const dataOfUid = snapshot.data() as CLCUser;
            functions.logger.info('dataOfUid', dataOfUid);
            response.send(dataOfUid);
        });
        promiseOfUserInvoked.catch(error => {
            response.status(500).send(error);
        });
    } else {
        response.status(500).send('UID олдсонгүй.');
    }
});

export const getDivUserList = functions.https.onRequest((request, response) => {
    if (request.body.uid) {
        const uid = request.body.uid as string;
        const userList = new Array;

        const promiseOfUser = admin.firestore().collection('clc_user').doc(uid).get();
        const promiseOfUserInvoked = promiseOfUser.then(snapshot => {

            const dataOfUid = snapshot.data() as CLCUser;
            functions.logger.info('dataOfUid', dataOfUid);
            const promiseOfMenu = admin.firestore().collection('clc_division').doc(dataOfUid.division).collection('user').get();
            const promiseOfMenuInvoked = promiseOfMenu.then(snapshot1 => {
                for (const i of snapshot1.docs) {
                    userList.push(i.data() as DetailUser);
                }
                response.send(userList);
            });
            promiseOfMenuInvoked.catch(error => {
                console.log(error);
                response.status(500).send(error);
            });

        });
        promiseOfUserInvoked.catch(error => {
            response.status(500).send(error);
        });
    } else {
        response.status(500).send('UID олдсонгүй.');
    }
});

export const getDivUserUIDs = functions.https.onRequest((request, response) => {
    if (request.body.uid) {
        const uid = request.body.uid as string;
        const userList = new Array;

        const promiseOfUser = admin.firestore().collection('clc_user').doc(uid).get();
        const promiseOfUserInvoked = promiseOfUser.then(snapshot => {
            const dataOfUid = snapshot.data() as CLCUser;
            functions.logger.info('dataOfUid', dataOfUid);

            const promiseOfUserList = admin.firestore().collection('clc_division').doc(dataOfUid.division).collection('user');

            if (dataOfUid.permission === 'admin' || dataOfUid.permission === 'director') {
                promiseOfUserList.get().then(snapshot1 => {
                    functions.logger.info('snapshot1.docs[0].data()', snapshot1.docs[0].data());
                    for (const i of snapshot1.docs) {
                        userList.push(i.data() as UIDName);
                        userList[userList.length - 1]['docID'] = i.id;
                    }
                    response.send(userList);
                }).catch(error => {
                    response.status(500).send(error);
                });
            } else if (dataOfUid.permission === 'user') {
                promiseOfUserList.doc(uid).get().then(snapshot1 => {
                    userList.push(snapshot1.data() as UIDName);
                    userList[0]['docID'] = snapshot1.id;

                    response.send(userList);
                }).catch(error => {
                    response.status(500).send(error);
                });
            }

        });
        promiseOfUserInvoked.catch(error => {
            response.status(500).send(error);
        });
    }
});

export const getAttendanceList = functions.https.onRequest((request, response) => {
    if (request.body.uid) {
        const uid = request.body.uid as string;
        const attendanceList = new Array;

        const promiseOfAttendance = admin.firestore().collection('clc_attendance').doc(uid).collection('attendance').get();
        const promiseOfAttendanceInvoked = promiseOfAttendance.then(snapshot => {

            functions.logger.info('snapshot.docs[0]', snapshot.docs[0].data());
            for (const i of snapshot.docs) {
                attendanceList.push(i.data() as Attendance);
                attendanceList[attendanceList.length - 1]['att_date'] = i.id;
            }
            response.send(attendanceList);
        });
        promiseOfAttendanceInvoked.catch(error => {
            response.status(500).send(error);
        });
    } else {
        response.status(500).send('UID олдсонгүй.');
    }
});

export const saveAttendance = functions.https.onRequest((request, response) => {
    functions.logger.info('request.body', request.body);
    const defaultStartTime = '08:00';
    const defaultEndTime = '17:00';

    if (request.body.uid && request.body.att_date && request.body.att_start_time) {
        const uid = request.body.uid as string;
        const data = {
            att_start_time: request.body.att_start_time,
            att_end_time: request.body.att_end_time,
            att_awanting_minutes: 0,
        };

        const dateString = (request.body.att_date as string);
        const dateStartDefault = new Date(dateString.substring(0, 4).concat('-',
            dateString.substring(4, 6),
            '-',
            dateString.substring(6, 8),
            'T',
            defaultStartTime,
            ':00'));
        const dateEndDefault = new Date(dateString.substring(0, 4).concat('-',
            dateString.substring(4, 6),
            '-',
            dateString.substring(6, 8),
            'T',
            defaultEndTime,
            ':00'));
        const dateStart = new Date(dateString.substring(0, 4).concat('-',
            dateString.substring(4, 6),
            '-',
            dateString.substring(6, 8),
            'T',
            request.body.att_start_time,
            ':00'));
        const dateEnd = new Date(dateString.substring(0, 4).concat('-',
            dateString.substring(4, 6),
            '-',
            dateString.substring(6, 8),
            'T',
            request.body.att_end_time,
            ':00'));
        let diffMs = 0;
        if (dateStart.getTime() > dateStartDefault.getTime()) {
            diffMs = dateStart.getTime() - dateStartDefault.getTime();
        }

        if (dateEnd.getTime() < dateEndDefault.getTime()) {
            diffMs += (dateEndDefault.getTime() - dateEnd.getTime());
        }

        functions.logger.info('diffMs', diffMs);
        let diffMins = 0;
        if (diffMs !== 0) {
            diffMins = (diffMs / 60000);
        }
        data.att_awanting_minutes = diffMins;

        const promiseOfUser = admin.firestore()
            .collection('clc_attendance')
            .doc(uid).collection('attendance')
            .doc(request.body.att_date).set(data);
        promiseOfUser.then(() => {
            response.send(request.body.att_date);
        }).catch(error => {
            response.status(500).send(error);
        });

    } else {
        response.status(500).send('Мэдээлэл олдсонгүй.');
    }
});

export const saveRequestOfVacation = functions.https.onRequest((request, response) => {
    functions.logger.info('request.body', request.body);

    const requestAttendance = request.body as RequestAttendance;

    // let lastId;

    if (request.body.UID) {
        const promiseOfLastId = admin.firestore().collection('clc_vacation').doc('lastId').get();
        promiseOfLastId.then(snapshot => {
            functions.logger.info('snapshot.data()', snapshot.data());
            const lastId = (snapshot.data() as LastIdOfVacation).count + 1;

            const dateStartDefault = requestAttendance.startDate.substring(0, 10).replace(/-/g, '');
            functions.logger.info('dateStartDefault', dateStartDefault);

            admin.firestore().collection('clc_vacation').doc(String(lastId)).set(requestAttendance).then(reqOfSave => {
                admin.firestore().collection('clc_attendance').doc(requestAttendance.UID).collection('attendance').doc(dateStartDefault).get().then(resDet => {
                    const attendance = resDet.data() as Attendance;
                    attendance.ref = 'clc_vacation/'.concat(String(lastId));
                    admin.firestore().collection('clc_attendance').doc(requestAttendance.UID).collection('attendance').doc(dateStartDefault).set(attendance).then(success => {
                        admin.firestore().collection('clc_vacation').doc(String(lastId)).set({'count': lastId}).then(() => {
                            response.send('Амжилттай');
                        }).catch(error => {
                            response.status(500).send(error);
                        });
                    }).catch(error => {
                        response.status(500).send(error);
                    });
                }).catch(error => {
                    response.status(500).send(error);
                });

            }).catch(error => {
                response.status(500).send(error);
            });

        }).catch(error => {
            response.status(500).send(error);
        });
    } else {
        response.status(500).send('Мэдээлэл олдсонгүй.');
    }
});

export const detailVacation = functions.https.onRequest((request, response) => {
    functions.logger.info('request.body.ref', request.body.ref);
    const ref = String(request.body.ref);

    const refSplitten = String(request.body.ref).split('/');

    if (ref != null && ref !== '') {
        if (refSplitten.length > 1) {
            const promiseOfLastId = admin.firestore().collection('clc_vacation').doc(refSplitten[1]).get();
            promiseOfLastId.then(snapshot => {
                functions.logger.info('snapshot.data()', snapshot.data());
                response.send(snapshot.data());
            }).catch(error => {
                response.status(500).send(error);
            });
        } else {
            response.status(500).send('Алдаатай хүсэлт.');
        }
    } else {
        response.status(500).send('Мэдээлэл олдсонгүй.');
    }
});

export const acceptVacation = functions.https.onRequest((request, response) => {
    functions.logger.info('request.body.ref', request.body.ref);
    const ref = String(request.body.ref);
    const refSplitten = String(request.body.ref).split('/');    

    if (ref != null && ref !== '') {
        if (refSplitten.length > 1) {
            let detailVacation;
            let startDate;

            const promiseOfLastId = admin.firestore().collection('clc_vacation').doc(refSplitten[1]).get();
            promiseOfLastId.then(snapshot => {
                detailVacation = snapshot.data() as RequestAttendance;
                startDate = detailVacation.startDate.substring(0, 10).replace(/-/g, '');
                admin.firestore().collection('clc_attendance').doc(detailVacation.UID).collection('attendance').doc(startDate).get().then(snapshot => {
                    
                }).catch(error => {
                    response.status(500).send(error);
                })
            }).catch(error => {
                response.status(500).send(error);
            });
        } else {
            response.status(500).send('Алдаатай хүсэлт.');
        }
    } else {
        response.status(500).send('Мэдээлэл олдсонгүй.');
    }
}


